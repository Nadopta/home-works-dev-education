package com.github.calc;

public class CalcMath {

    public static double calc(double a, double b, String op) {
        double result = 0;
        switch (op) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            case "/":
                result =  a / b;
                break;
        }
        return result;
    }

}
