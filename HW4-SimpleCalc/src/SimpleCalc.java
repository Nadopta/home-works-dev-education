package com.github.calc;

import javax.swing.*;
import java.awt.Color;

public class SimpleCalc extends JFrame {

    private JTextField tfFirstNum;

    private JTextField tfOperation;

    private JTextField tfSecondNum;

    private JTextField tfResult;

    public SimpleCalc(ActionManager ac) {
        super("Калькулятор");

        JPanel content = new JPanel();
        content.setLayout(null);
        content.setBackground(new Color(221,226,206));

        JLabel lblFirstNum = new JLabel("Первое число");
        lblFirstNum.setBounds(12, 5, 195, 21);

        JLabel lblOperation = new JLabel("Операция");
        lblOperation.setBounds(38, 55, 195, 21);

        JLabel lblSecondNum = new JLabel("Второе число");
        lblSecondNum.setBounds(15, 30, 195, 21);


        JLabel lblResult = new JLabel("Результат");
        lblResult.setBounds(40, 130, 195, 21);


        this.tfFirstNum = new JTextField(20);
        tfFirstNum.setBounds(100, 5, 120, 21);

        this.tfOperation = new JTextField(1);
        tfOperation.setBounds(100, 55, 120, 21);

        this.tfSecondNum = new JTextField(20);
        tfSecondNum.setBounds(100, 30, 120, 21);

        this.tfResult = new JTextField(20);
        tfResult.setBounds(100, 130, 120, 21);

        JButton btnCalc = new JButton("Посчитать");
        btnCalc.setBounds(10, 90, 250, 21);

        ac.setTfFirstNum(this.tfFirstNum);
        ac.setTfSecondNum(this.tfSecondNum);
        ac.setTfOperation(this.tfOperation);
        ac.setTfResult(this.tfResult);

        btnCalc.addActionListener(ac.getBtnEqActionListener());

        content.add(lblFirstNum);
        content.add(lblOperation);
        content.add(lblSecondNum);
        content.add(tfFirstNum);
        content.add(tfOperation);
        content.add(tfSecondNum);
        content.add(btnCalc);

        content.add(lblResult);
        content.add(tfResult);

        setSize(280, 230);
        setLocationRelativeTo(null);
        setResizable(false);


        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(content);
    }
}