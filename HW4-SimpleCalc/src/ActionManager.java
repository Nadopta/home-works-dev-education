package com.github.calc;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionManager {

    private JTextField tfFirstNum;

    private JTextField tfOperation;

    private JTextField tfSecondNum;

    private JTextField tfResult;

    private final BtnEqActionListener btnEqActionListener = new BtnEqActionListener();

    private class BtnEqActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int firstNum = Integer.parseInt(tfFirstNum.getText());
            String op = tfOperation.getText();
            int secondNum = Integer.parseInt(tfSecondNum.getText());
            double result = CalcMath.calc(firstNum, secondNum, op);
            tfResult.setText(String.valueOf(result));
        }
    }

    public BtnEqActionListener getBtnEqActionListener() {
        return btnEqActionListener;
    }

    public void setTfFirstNum(JTextField tfFirstNum) {
        this.tfFirstNum = tfFirstNum;
    }

    public void setTfOperation(JTextField tfOperation) {
        this.tfOperation = tfOperation;
    }

    public void setTfSecondNum(JTextField tfSecondNum) {
        this.tfSecondNum = tfSecondNum;
    }

    public void setTfResult(JTextField tfResult) {
        this.tfResult = tfResult;
    }
}
