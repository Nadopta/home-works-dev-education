import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Collection {

    public static void main(String[] args) {

        List<Student> stud = new ArrayList<>() {{
            add(new Student(1, "Alex", "Bulkin", 2000, "Kiev", "02","RTF","2","jwh18"));
            add(new Student(2, "Andrew", "Vasechkin", 2003, "Kiev","03", "Ch", "1","r7"));
            add(new Student(3, "Anton", "Drehler", 2001, "Kirovograd", "01","F","3","F23"));
            add(new Student(4, "Boris", "Enotik", 2002, "Lviv", "777", "L", "1","L46"));
            add(new Student(5, "Vasya", "Volkov", 2004, "Kiev", "123", "RTF","2","jwh18"));
            add(new Student(6, "Alex", "Smirnov", 2001, "Lviv", "16","Ch","3", "r6"));
            add(new Student(7, "Alina", "Petushikhina", 2000, "Kiev","04","L","2","J7"));
            add(new Student(8, "Genadiy", "Poroshenko", 1999, "Kirovograd","07", "F", "4", "f16"));
            add(new Student(9, "Dmytro", "Zukerman", 1999, "Lviv", "08", "Ch","3", "k2"));
            add(new Student(10, "Semen", "Pavlenko", 2000, "Kiev","15","F","1","f16"));
            add(new Student(11, "Mykola", "Petrenko", 2001, "Kharkov","16","F","2","F23"));
            add(new Student(12, "Timofey", "Milovanov", 2002, "Lviv","162","Ch","3","r6"));
            add(new Student(13, "Timur", "Khachanohcha", 2003, "Kharkov","228","Ch","1","r7"));
            add(new Student(14, "Oleg", "Belyi", 2000, "Chernigov","882","RTF","2","abc"));
            add(new Student(15, "Yaroslav", "Rozumkov", 1999, "Chernigov","911","RTF","3","abc"));
            add(new Student(16, "Solomon", "Goldman", 2003, "Odessa","112","L","1","J7"));
            add(new Student(17, "Samuil", "Levin", 2002, "Odessa","245","L","2","j8"));
            add(new Student(18, "John", "Connor", 2001, "Kirovograd","007","TEF","3","t1"));
            add(new Student(19, "Konstantin", "Semenov", 1991, "Kiev","321","TEF","4","t1"));



        }};

        List<Student> studRTF= stud.stream().filter((s)->s.getFaculty().equals("RTF")).collect(Collectors.toList());
        long numOfRTFStuds= studRTF.stream().count();



        List<Student> studyounger=  stud.stream().filter((s)->
                s.getBirthYear()>2001).collect(Collectors.toList());

      long numOfChStuds= stud.stream().filter((s)->s.getFaculty().equals("Ch")).count();

        System.out.println(numOfChStuds);

    }




}
