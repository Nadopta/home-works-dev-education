import javax.swing.*;

public class PaintFilePanel extends JPanel {

    public static int th=1;

    public PaintFilePanel() {
        setLayout(null);
        setBounds(820, 10, 200, 200);
        String[] thck={"1","2","3","4","5"};

        JButton btnSave = new JButton("Save");
        JButton btnOpen = new JButton("Open");

        JComboBox thickness= new JComboBox(thck);
        thickness.setBounds(10,50,100,30);

        btnSave.setBounds(10, 10, 70, 30);
        btnOpen.setBounds(90, 10, 70, 30);

        add(btnSave);
        add(btnOpen);
        add(thickness);

        this.th=thickness.getItemCount();

        setVisible(Boolean.TRUE);
    }

}