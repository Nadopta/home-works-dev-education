import java.awt.*;

public class ApplicationPaint {
    public static void main(String[] args) {
        new PaintFrame(new PaintPanel(), new PaintFilePanel())
                .getContentPane().setBackground(Color.BLACK);
    }
}
