import java.awt.*;
import java.util.Objects;

public class CustomLine {

    private int firstX, firstY, lastX, lastY;

    private int color;

    private int width;

    public CustomLine(int firstX, int firstY, int lastX, int lastY) {
        this.firstX = firstX;
        this.firstY = firstY;
        this.lastX = lastX;
        this.lastY = lastY;
        this.color = Color.BLACK.getRGB();
        this.width = PaintFilePanel.th;
    }

    public int getFirstX() {
        return firstX;
    }

    public int getFirstY() {
        return firstY;
    }

    public int getLastX() {
        return lastX;
    }

    public int getLastY() {
        return lastY;
    }

    public int getColor() {
        return color;
    }

    public int getWidth() {
        return width;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomLine that = (CustomLine) o;
        return firstX == that.firstX &&
                firstY == that.firstY &&
                lastX == that.lastX &&
                lastY == that.lastY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstX, firstY, lastX, lastY);
    }

    @Override
    public String toString() {
        return "CustomLine{" +
                "firstX=" + firstX +
                ", firstY=" + firstY +
                ", lastX=" + lastX +
                ", lastY=" + lastY +
                '}';
    }
}
