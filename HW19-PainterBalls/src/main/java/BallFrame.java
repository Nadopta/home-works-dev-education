import javax.swing.*;
import java.awt.*;

public class BallFrame extends JFrame {
    public BallFrame() throws HeadlessException {
        setLayout(null);
        setBackground(Color.BLACK);
        setVisible(Boolean.TRUE);
        setSize(1000,800);
        BallsPanel bp = new BallsPanel();
        bp.setBackground(Color.WHITE);
        bp.setBounds(10, 10, 800,600);
        add(bp);
        bp.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(1)));
    }
}
