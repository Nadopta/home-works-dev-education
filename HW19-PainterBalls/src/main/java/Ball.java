import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private Point point;
    private Color clr;
    private Random rnd=new Random();
    private int dx;
    private int dy;
    private int d;

    public Ball(Point point) {
        this.point=point;
        int red=rnd.nextInt(255);
        int green=rnd.nextInt(255);
        int blue=rnd.nextInt(255);
        this.clr=new Color(red,green,blue);
        this.dx=rnd.nextInt(100)-50;
        this.dy=rnd.nextInt(100)-50;
        this.d=(rnd.nextInt(75))+1;
        setSize(this.d,this.d);
        setOpaque(Boolean.FALSE);
    }

    private void move() {
      JPanel pl=  (JPanel) getParent();
      if (this.point.x<=0 || this.point.x+this.d>=pl.getWidth()) {
          dx*=-1;
      }
        if (this.point.y<=0 || this.point.y+this.d>=pl.getHeight()) {
            dy*=-1;
        }

        this.point.translate(dx,dy);

        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(clr);
        g2d.fillOval(0,0,this.d,this.d);
    }

    @Override
    public void run() {

        try {
            while(true) {
                move();
                Thread.sleep(200);
            }
        }
        catch (InterruptedException e) {
           Thread.currentThread().interrupt();
        }
    }
}
