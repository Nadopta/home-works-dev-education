public class CustomException extends Exception {

    private int num;
    private String message;

    public int getNumber() {
        return this.num;
    }

    @Override
    public String getMessage() {
        return this.message;
    }


    public CustomException(String msg, int num) {
        this.message=msg;
        this.num=num;
    }
}
