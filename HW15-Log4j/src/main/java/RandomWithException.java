public class RandomWithException {
    public static int getRandom() throws CustomException {
        int r=RandomGen.randomGen();

      if (r<=5) {
            throw new CustomException("Generated number - ", r);
        }
      return r;

    }
}
