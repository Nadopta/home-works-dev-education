public class Dayname {
    public static String dayname(int i) {
        if (i == 1) return "Monday";
        if (i == 2) return "Tuesday";
        if (i == 3) return "Wednesday";
        if (i == 4) return "Thursday";
        if (i == 5) return "Friday";
        if (i == 6) return "Saturday";
        if (i == 7) return "Sunday";
        return "Day number out of range";
    }
}
