public class PartTwo {


    public static String intToString (int i) {
        return Integer.toString(i);
    }

    public static String doubleToString(double d) {
        return Double.toString(d);
    }

    public static int stringToInt(String str) {
        return Integer.parseInt(str);
    }

    public static double stringToDouble(String str) {
        return Double.parseDouble(str);
    }
}
