public class PartThree {

    public static int shortestWordLength(String str) {
        if (str == null || "".equals(str)) throw new NullPointerException("String is null");

        String newStr = str.replaceAll("[,-:;.!?/]", "");
        String words[] = newStr.split(" ");
        int min = words[0].length();
        for (int i = 0; i < words.length ; i++) {
            if (words[i].length() < min) {
                min = words[i].length();
            }
            }
        return min;
    }

    public static String[] wordDollar(String[] strs, int len) {
        String[] newStrs= new String[strs.length];
        for (int i=0; i<strs.length; i++) {
            if (strs[i].length()==len) {
                if (len<=3) {
                    newStrs[i]="$";

                } else newStrs[i]=strs[i].substring(0, (strs[i].length()-3))+"$";
            } else newStrs[i]=strs[i];
        }
        return newStrs;
    }

    public static int wordsCounter(String str) {
        return str.split(" ").length;
    }

    public static String reverser(String x) {
        String y="";
        char[] charArray = x.toCharArray();
        for (int i=(charArray.length-1); i>=0; i--) {
            y=y+charArray[i];
        }
        return y;
    }

    public static String lastWordDeleter(String x) {
        String[] tmp=x.split(" ");
        String output="";
        for (int i=0; i<tmp.length-1; i++) {
            output=output+" "+tmp[i];
        }
        return output.strip();
    }

    public static String uniqueSymbols(String str) {
        if(str == null || "".equals(str)){
            throw new NullPointerException("String is null");
        }
        char[] symbols = str.toCharArray();
        String chars = "";
        for(int i = 0; i < symbols.length; i++){
            if(chars.indexOf(symbols[i]) == -1){
                chars = chars + symbols[i];
            }
        }
        return chars;

    }

    public static String stringCutter (String str, int startPos, int len) {
        if (startPos<(str.length()-1)) {
            if (startPos+len<str.length()-1) {
                return ((str.substring(0,startPos ))+(str.substring(startPos+len)));
            } else return (str.substring(0,startPos ));
        } else return str;
        }

    public static String spaceAdder(String line) {
        char[] lineInChars = line.toCharArray();
        String newLine="";
        for (int i=0; (i< ((lineInChars.length)-1)); i++) {
            newLine+=lineInChars[i];
            if ((lineInChars[i]=='.') || (lineInChars[i]==',') || (lineInChars[i]==':') || (lineInChars[i]=='!') || (lineInChars[i]=='?')) {
                if   (lineInChars[i+1]!=' ') {
                    newLine+=' ';
                }
            }
        }
        newLine+=lineInChars[lineInChars.length-1];

        if ((lineInChars[lineInChars.length-1]=='.') || (lineInChars[lineInChars.length-1]==',') || (lineInChars[lineInChars.length-1]==':') || (lineInChars[lineInChars.length-1]=='!') || (lineInChars[lineInChars.length-1]=='?')) {
            newLine+=' ';
        }
        return newLine;
    }
}
