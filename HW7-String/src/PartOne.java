public class PartOne {
    public static void main(String[] args) {
       AToZLowerCase();
       ZToALowerCase();
       AToZUpperCase();
       RusAlphabet();
        Digits();
    }

    public static void AToZLowerCase() {
        char ch;
        for (ch = 'a'; ch <= 'z'; ch++){
            System.out.print(ch);
        }
        System.out.print("\n");

    }

    public static void ZToALowerCase(){
        char ch;
        for (ch = 'z'; ch >= 'a'; ch--) {
            System.out.print(ch);
        }
        System.out.print("\n");

    }

    public static void AToZUpperCase() {
        char ch;
        for(ch = 'A'; ch <= 'Z'; ch++) {
            System.out.print(ch);
        }
        System.out.print("\n");

    }

    public static void RusAlphabet(){
        char ch;
        for(ch = 'а'; ch <= 'я'; ch++){
            System.out.print(ch);
        }
        System.out.print("\n");

    }

    public static void Digits() {
        char ch;
        for(ch = '0'; ch <= '9'; ch++){
            System.out.print(ch);
        }
        System.out.print("\n");

    }
}
