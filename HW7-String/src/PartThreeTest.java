
import org.junit.Assert;
import org.junit.Test;

public class PartThreeTest {
    @Test
    public void smallestWord(){
        String data = "Blows your mind drastically, fantastically";
        int exp = 4;
        int act = PartThree.shortestWordLength(data);
        Assert.assertEquals(exp, act);
    }


    @Test
    public void changeSymbols() {
        String[] data = {"Blows", "your", "mind", "drastically", "fantastically"};
        String[] exp =  {"Blows", "y$", "m$", "drastically", "fantastically"};
        String[] act = PartThree.wordDollar(data, 4);
        Assert.assertArrayEquals(exp, act);
    }


    @Test
    public void spaceAdder(){
        String data = "Add space after, punctuation symbols,if there is none.";
        String act = PartThree.spaceAdder(data);
        String exp = "Add space after, punctuation symbols, if there is none.";
        Assert.assertEquals(exp, act);
    }

    @Test
    public void uniqueSymbols(){
        String data = "qweqwertyyrty";
        String act = PartThree.uniqueSymbols(data);
        String exp = "qwerty";
        Assert.assertEquals(exp, act);
    }

    @Test
    public void wordsCounter(){
        String data = "Blows your mind drastically, fantastically";
        int exp = 5;
        int act = PartThree.wordsCounter(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void stringCutter(){
        String data = "Blows your mind drastically, fantastically";
        String exp = "Blows your mind fantastically";
        String act = PartThree.stringCutter(data, 15, 13);
        Assert.assertEquals(exp,act);
    }

    @Test
    public void reverseString(){
        String data = "qwerty";
        String exp = "ytrewq";
        String act = PartThree.reverser(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void lastWordDeleter(){
        String data = "Blows your mind drastically, fantastically";
        String act = PartThree.lastWordDeleter(data);
        String exp = "Blows your mind drastically,";
        Assert.assertEquals(exp, act);
    }
}
}
