
import org.junit.Assert;
import org.junit.Test;

public class PartTwoTest {

    @Test
    public void IntToString(){
        int toConvert = 123;
        String exp = "123";
        String act = PartTwo.intToString(toConvert);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void doubleToString(){
        double toConvert = 14.88;
        String exp = "14.88";
        String act = PartTwo.doubleToString(toConvert);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void stringToInt){
        String toConvert = "8520";
        int exp = 8520;
        int act =PartTwo.stringToInt(toConvert);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void stringToDouble(){
        String toConvert = "3.14";
        double exp = 3.14;
        double act = PartTwo.stringToDouble(toConvert);
        double delta = 0.01;
        Assert.assertEquals(exp, act, delta);
    }
}
