import javax.swing.*;
import java.awt.Color;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.sound.sampled.*;

import java.awt.event.*;

public class ConverterFrame extends JFrame {

    public ConverterFrame() {
        super("Converter");

        JPanel content = new JPanel();
        content.setLayout(null);
        content.setBackground(new Color(255,242,0));

        JLabel lblFirstNum = new JLabel("Value");
        lblFirstNum.setBounds(12, 5, 195, 21);

        JLabel lblResult = new JLabel("Converted value");
        lblResult.setBounds(20, 130, 195, 21);

        String convertTypes[]={"--Choose type of conversion--","--TIME--","sec to min","min to sec","sec to hours","hours to sec",
                "sec to day","day to sec", "sec to week", "week to sec", "sec to month","month to sec","sec to years",
                "years to sec",
                "--WEIGHT--",
                "kg to g","g to kg","kg to carat", "carat to kg", "kg to eng pound", "eng pound to kg", "kg to pound",
                "pound to kg", "kg to stone", "stone to kg", "kg to rus pound", "rus pound to kg",
                "--VOLUME--",
                "l to m^3", "m^3 to l", "l to gallon", "gallon to l", "l to pint", "pint to l", "l to quart", "quart to l",
                "l to barrel", "barrel to l", "l to cubic foot", "cubic foot to l", "l to cubic inch", "cubic inch to l",
                "--LENGTH--",
                "m to km", "km to m", "m to mile", "mile to m", "m to nautical mile", "nautical mile to m", "m to cable",
                "cable to m", "m to league", "league to m", "m to foot", "foot to m","m to yard", "yard to m",
                "--TEMPERATURE--",
                "C to K", "K to C", "C to F", "F to C", "C to Re", "Re to C", "C to Ro" , "Ro to C", "C to Ra", "Ra to C",
                "C to N", "N to C", "C to D", "D to C" };

        final JComboBox conversionType = new JComboBox(convertTypes);
        conversionType.setBounds(20, 30, 300,20);

        JTextField tfValue = new JTextField(20);
        tfValue.setBounds(100, 5, 200, 21);
        tfValue.setText("0");


        JTextField tfResult = new JTextField(20);
        tfResult.setBounds(150, 130, 200, 21);


        JButton btnCalc = new JButton("Convert");
        btnCalc.setBounds(10, 90, 250, 21);

        content.add(lblFirstNum);
        content.add(conversionType);
        content.add(tfValue);
        content.add(btnCalc);
        content.add(lblResult);
        content.add(tfResult);
        setSize(500,500);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(content);
        Toolkit.getDefaultToolkit().beep();
        System.out.flush();

        btnCalc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double val=Double.valueOf(tfValue.getText());
                int op=conversionType.getSelectedIndex();
                double newValue = ConverterMath.convert(val,op);
                String output=Double.toString(newValue);


                tfResult.setText(output);
                if (op==0 || op == 1 || op == 14 || op == 27 || op == 42 || op == 57) {
                    Exceptions al = new Exceptions();
                   // al.setVisible(true);

                }
            }
        });

    }





}
