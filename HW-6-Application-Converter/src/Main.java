import javax.swing.*;




public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Throwable thrown) {
            thrown.printStackTrace();
        }
        ConverterFrame abt = new ConverterFrame();
        abt.setVisible(true);

    }

}
