public class ConverterMath {
    //
    public static double convert(double a,  int op) {
        double result = 0;
        switch (op) {
            case 0:
                result = a ;
                break;
            case 1:
                result = a ;
                break;
            case 14:
                result = a;
                break;
            case 27:
                result =  a;
                break;
            case 42:
                result =  a;
                break;
            case 57:
                result =  a;
                break;
            // exception when user selected wrong option

            //time
            case 2:
                result =  a/60;
                break;
            case 3:
                result =  a*60;
                break;
            case 4:
                result =  a/3600;
                break;
            case 5:
                result =  a*3600;
                break;
            case 6:
                result =  a/86400;
                break;
            case 7:
                result =  a*86400;
                break;
            case 8:
                result =  a/(86400*7);
                break;
            case 9:
                result =  a*86400*7;
                break;
            case 10:
                result =  a/2629744;
                break;
            case 11:
                result =  a*2629744;
                break;
            case 12:
                result =  a/31556926;
                break;
            case 13:
                result =  a*31556926;
                break;

            //weight

            case 15:
                result =  a*1000;
                break;
            case 16:
                result =  a/1000;
                break;
            case 17:
                result =  a*5000;
                break;
            case 18:
                result =  a/5000;
                break;
            case 19:
                result =  a/0.45359237;
                break;
            case 20:
                result =  a*0.45359237;
                break;
            case 21:
                result =  a/0.45359237;
                break;
            case 22:
                result =  a*0.45359237;
                break;
            case 23:
                result =  a/6.35029318;
                break;
            case 24:
                result =  a*6.35029318;
                break;
            case 25:
                result =  a/0.409512037;
                break;
            case 26:
                result =  a*0.409512037;
                break;

            //volume

            case 28:
                result =  a/1000;
                break;
            case 29:
                result =  a*1000;
                break;
            case 30:
                result =  a/3.78541178;
                break;
            case 31:
                result =  a*3.78541178;
                break;
            case 32:
                result =  a/0.473176473;
                break;
            case 33:
                result =  a*0.473176473;
                break;
            case 34:
                result =  a/0.946352946;
                break;
            case 35:
                result =  a*0.946352946;
                break;
            case 36:
                result =  a/117.347765;
                break;
            case 37:
                result =  a*117.347765;
                break;
            case 38:
                result =  a/28.3168466;
                break;
            case 39:
                result =  a*28.3168466;
                break;
            case 40:
                result =  a/0.016387064;
                break;
            case 41:
                result =  a*0.016387064;
                break;

            //length

            case 43:
                result =  a/1000;
                break;
            case 44:
                result =  a*1000;
                break;
            case 45:
                result =  a/1609.344;
                break;
            case 46:
                result =  a*1609.344;
                break;
            case 47:
                result =  a/1852;
                break;
            case 48:
                result =  a*1825;
                break;
            case 49:
                result =  a/219.456;
                break;
            case 50:
                result =  a*219.456;
                break;
            case 51:
                result =  a/5556;
                break;
            case 52:
                result =  a*5556;
                break;
            case 53:
                result =  a/0.3048;
                break;
            case 54:
                result =  a*0.3048;
                break;
            case 55:
                result =  a/0.9144;
                break;
            case 56:
                result =  a*0.9144;
                break;

            //temp

            case 58:
                result =  a+273.15;
                break;
            case 59:
                result =  a-273.15;
                break;
            case 60:
                result =  (a*9/5)+32;
                break;
            case 61:
                result =  (a-32)*5/9;
                break;
            case 62:
                result =  a*0.8;
                break;
            case 63:
                result =  a*1.25;
                break;
            case 64:
                result =  (a*21/40)+7.5;
                break;
            case 65:
                result =  (a-7.5)*40/21;
                break;
            case 66:
                result =  (a+273.15)*9/5;
                break;
            case 67:
                result =  (a*5/9)-273.15;
                break;
            case 68:
                result =  a*33/100;
                break;
            case 69:
                result =  a*100/33;
                break;
            case 70:
                result =  (100-a)*3/2;
                break;
            case 71:
                result =  100-(2*a/3);
                break;

        }
        return result;
    }

}