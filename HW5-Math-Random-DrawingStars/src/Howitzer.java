import java.util.Scanner;

public class Howitzer {
    public static void main(String[] args ) {
        Scanner ang=new Scanner(System.in);
        System.out.println("Input angle");
        double an=ang.nextDouble();

        Scanner rOrDegr=new Scanner(System.in);
        System.out.println("Radians or Degrees? (R/D)");
        String units=rOrDegr.nextLine();

        Scanner v=new Scanner(System.in);
        System.out.println("Input muzzle velocity");
        double vel=v.nextDouble();

        if (units.equals("R")) {
            System.out.println("Distance is "+distanceRadians(an, vel));
        }
        if (units.equals("D")) {
            System.out.println("Distance is "+distanceDegrees(an, vel));
        }


    }

    public static double distanceDegrees (double a, double velocity) {
        velocity=velocity/3.6;
        double angl=Math.toRadians(a);
        double sinAngl=Math.sin(angl);
        double cosAngl=Math.cos(angl);
        double distance=2*velocity*sinAngl*velocity*cosAngl/9.81;
        return distance;
    }

    public static double distanceRadians (double a, double velocity) {
        velocity=velocity/3.6;
        double sinAngl=Math.sin(a);
        double cosAngl=Math.cos(a);
        double distance=2*velocity*sinAngl*velocity*cosAngl/9.81;
        return distance;

    }
}
