import java.util.Scanner;
public class InAreaCheck {
    public static void main(String[] args) {
        System.out.println("input x");
        Scanner xin=new Scanner(System.in);
        double x=xin.nextDouble();
        System.out.println("input y");
        Scanner yin=new Scanner(System.in);
        double y=yin.nextDouble();
        System.out.println(inAreaCheck(x,y));

    }
    public static byte inAreaCheck (double x , double y) {
        if (y<-1 || y> 2) return 0;
        if (x<-2 || x>2) return 0;
        if (x>0 && y>x) return 0;
        if (x<0 && y>-x) return 0;
        if (x<0 && y< ((1.5*(-x))-1)) return 0;
        if (x>0 && y< ((1.5*x)-1)) return 0;
        return 1;

     }
}
