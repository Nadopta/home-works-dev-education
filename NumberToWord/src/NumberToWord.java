public class NumberToWord {
    public static String numToWord (int j) {
        float a,b,c;
        String propis="";
        c=j%10;
        b=(j%100-c)/10;
        a=(j%1000-10*b-c)/100;

        if (a == 1) propis +="One hundred";
        if (a == 2) propis+="Two hundred";
        if (a == 3) propis+="Three hundred";
        if (a == 4) propis+="Four hundred";
        if (a == 5) propis+="Five hundred";
        if (a == 6) propis+="Six hundred";
        if (a == 7) propis+="Seven hundred";
        if (a == 8) propis+="Eight hundred";
        if (a == 9) propis+="Nine hundred";
        if (b == 1) {
            if (c==0) propis+=" ten";
            if (c==1) propis+=" eleven";
            if (c==2) propis+=" twelve";
            if (c==3) propis+=" thirteen";
            if (c==4) propis+=" fourteen";
            if (c==5) propis+=" fifteen";
            if (c==6) propis+=" sixteen";
            if (c==7) propis+=" seventeen";
            if (c==8) propis+=" eighteen";
            if (c==9) propis+=" nineteen";
        }
        if (b == 2) propis+=" twenty";
        if (b == 3) propis+=" thirty";
        if (b == 4) propis+=" fourty";
        if (b == 5) propis+=" fifty";
        if (b == 6) propis+=" sixty";
        if (b == 7) propis+=" seventy";
        if (b == 8) propis+=" eighty";
        if (b == 9) propis+=" ninety";
        if (a==0 && b==0 && c==0) propis="Zero";
        if (b!=1) {
            if (c==1) propis+=" one";
            if (c==2) propis+=" two";
            if (c==3) propis+=" three";
            if (c==4) propis+=" four";
            if (c==5) propis+=" five";
            if (c==6) propis+=" six";
            if (c==7) propis+=" seven";
            if (c==8) propis+=" eight";
            if (c==9) propis+=" nine";

        }
        return (propis);


    }
}
