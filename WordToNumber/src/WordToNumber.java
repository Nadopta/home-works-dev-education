public class WordToNumber {
    public static int wordToNumber (String s) {
        int n=0;
        String c=s.toLowerCase();
        if (c.equals("zero")) return 0;

        String [] words = c.split(" ");

        for (int i=0;i<words.length; i++) {
            if (words[i].equals("hundred")) {
                if (words[i-1].equals("one")) n+=100;
                if (words[i-1].equals("two")) n+=200;
                if (words[i-1].equals("three")) n+=300;
                if (words[i-1].equals("four")) n+=400;
                if (words[i-1].equals("five")) n+=500;
                if (words[i-1].equals("six")) n+=600;
                if (words[i-1].equals("seven")) n+=700;
                if (words[i-1].equals("eight")) n+=800;
                if (words[i-1].equals("nine")) n+=900;
            }

        }
        if (words[words.length-1].equals("one")) n+=1;
        if (words[words.length-1].equals("two")) n+=2;
        if (words[words.length-1].equals("three")) n+=3;
        if (words[words.length-1].equals("four")) n+=4;
        if (words[words.length-1].equals("five")) n+=5;
        if (words[words.length-1].equals("six")) n+=6;
        if (words[words.length-1].equals("seven")) n+=7;
        if (words[words.length-1].equals("eight")) n+=8;
        if (words[words.length-1].equals("nine") )n+=9;
        if (words[words.length-1].equals("ten") )n+=10;
        if (words[words.length-1].equals("eleven")) n+=11;
        if (words[words.length-1].equals("twelve")) n+=12;
        if (words[words.length-1].equals("thirteen")) n+=13;
        if (words[words.length-1].equals("fourteen")) n+=14;
        if (words[words.length-1].equals("fifteen") )n+=15;
        if (words[words.length-1].equals("sixteen")) n+=16;
        if (words[words.length-1].equals("seventeen")) n+=17;
        if (words[words.length-1].equals("eighteen")) n+=18;
        if (words[words.length-1].equals("nineteen")) n+=19;
        if (words[words.length-1].equals("twenty") || words[words.length-2].equals("twenty" ) )n+=20;
        if (words[words.length-1].equals("thirty") || words[words.length-2].equals("thirty") ) n+=30;
        if (words[words.length-1].equals("fourty") || words[words.length-2].equals("fourty")) n+=40;
        if (words[words.length-1].equals("fifty") || words[words.length-2].equals("fifty")) n+=50;
        if (words[words.length-1].equals("sixty") || words[words.length-2].equals("sixty")) n+=60;
        if (words[words.length-1].equals("seventy") || words[words.length-2].equals("seventy")) n+=70;
        if (words[words.length-1].equals("eighty") || words[words.length-2].equals("eighty")) n+=80;
        if (words[words.length-1].equals("ninety") || words[words.length-2].equals("ninety")) n+=90;
        return n;








    }


}
